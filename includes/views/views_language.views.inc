<?php

/**
 * @file
 * Views hook implementations.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_language_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    foreach ($config as $item => $item_config) {
      if (isset($item_config['field']['handler']) && $item_config['field']['handler'] == 'views_handler_field_node_language') {
        $data[$table][$item]['field']['handler'] = 'views_language_handler_field_node_language_alter';
      }
    }
  }
  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function views_language_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_language') . '/includes/views/handlers',
    ),
    'handlers' => array(
      'views_language_handler_field_node_language_alter' => array(
        'parent' => 'views_handler_field_node_language',
      ),
    ),
  );
}
